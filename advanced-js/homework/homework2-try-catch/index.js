'use strict'

const books = [
  {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  }
];

function outputArray(arr) {
  const newArr = arr.filter(elem => {
    try {
      if (Object.keys(elem).length === 3) {
        return elem;
      } else {
        const subArr = Object.keys(elem);
        if (subArr[0] === 'author' && subArr[1] === 'name') {
          throw new Error('Missing field "price"');
        } else {
          throw new Error('Missing field "author"')
        }
      }
    } catch (e) {
      console.error(e)
    }
  });

  const ul = document.createElement('ul');
  newArr.forEach(elem => {
    const li = document.createElement('li');
    const low = Object.values(elem);
    li.innerText = low.join(',');
    ul.append(li);
  });
  return ul;
}

const div = document.querySelector('#root');

div.append(outputArray(books));