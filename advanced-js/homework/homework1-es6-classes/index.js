'use strict';

class Employee {
  constructor(name,age,salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }

  get name() {
    return this._name;
  }
  set name(name) {
      this._name = name;
  }

  get age() {
    return this._age;
  }
  set age (age) {
      this._age = age;
  }

  get salary() {
    return this._salary;
  }
  set salary (salary) {
    this._salary =  salary;
  }
}

class Programmer extends Employee{
  constructor(name,age,salary,lang) {
    super(
       name,
       age,
       salary,
    );
    this._lang = lang;
  }

  get salary(){
    return this._salary * 3;
  }
}

const programmer = new Programmer('Alex','47','1000','javaScript');
const programmerSecond = new Programmer('Ivan','50','800','java');

console.log(programmer);
console.log(programmerSecond);
console.log(programmer.salary)

