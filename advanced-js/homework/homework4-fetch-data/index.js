fetch('https://ajax.test-danit.com/api/swapi/films')
  .then(response => response.json())
  .then(films => {
    console.log(films);
    const filmsList = films.map(film => `<li>Episode ${film.episodeId} "${film.name}." ${film.openingCrawl}</li>`).join(' ');
    const ul = document.createElement('ul');
    ul.innerHTML = filmsList;
    const body = document.querySelector('body');
    body.prepend(ul);

    films.forEach((film) => {
      const div = document.createElement('div');
      const title = document.createElement('h2');
      title.innerText = `${film.name}`;
      const listCharacters = document.createElement('p');
      div.append(title);
      div.append(listCharacters);
      body.append(div);

      const filmCharacters = film.characters;
      filmCharacters.map(elem => {
        const url = elem;
        fetch(url)
          .then(response => response.json()
            .then(item => {
              const itemLi = ` ${item.name} .`
              listCharacters.append(itemLi);
            }))
      })
    })
  })