const icon = document.querySelector('.icon');
const btn = document.querySelector('.btn');
const err = document.querySelector('p');

icon.addEventListener('click', evt => {

    if (evt.target.classList.contains('fa-eye-slash')) {
        evt.target.classList.remove('fa-eye-slash');
        evt.target.classList.add('fa-eye');
        const label = evt.target.closest('label');
        const inputMy = label.querySelector('input');
        inputMy.setAttribute('type', 'password');
    } else {
        evt.target.classList.remove('fa-eye');
        evt.target.classList.add('fa-eye-slash');
        const label = evt.target.closest('label');
        const inputMy = label.querySelector('input');
        inputMy.setAttribute('type', 'text');
    }
});
btn.addEventListener('click', evt => {
    evt.preventDefault();

    const inputValue = icon.querySelectorAll('input');

    if (inputValue[0].value !== '' && inputValue[1].value !== ''
        && inputValue[0].value === inputValue[1].value) {
        err.classList.remove('error');
        err.classList.add('not-error');
        console.log('You are welcome');
    } else {
        err.classList.remove('not-error');
        err.classList.add('error');
    }
})
