const btnCollection = document.querySelectorAll('button');
window.addEventListener('keydown', ev => {
    for (elem of btnCollection) {
        if (elem.dataset.text === ev.code) {
            elem.classList.add('btn-action');
        } else {
            elem.classList.toggle('btn-action', false);
        }
    }
});