const showList = ((arr, parentElem = document.body) => {
    const arrTwo = arr.map(item => {
        if (Array.isArray(item)) {
            let arrSub = item.map(item => {
                return `<li>${item}</li>`;
            });
            return arrSub;
        } else {
            return `<li>${item}</li>`;
        }
    });

    const elem = document.createElement(parentElem);
    document.body.appendChild(elem);
    arrTwo.forEach(item => {
        if (Array.isArray(item)) {
            const ul = document.createElement('ul');
            elem.appendChild(ul);
            item.forEach(item => {
                ul.insertAdjacentHTML('afterbegin', item);

            });
        } else {
            elem.insertAdjacentHTML('afterbegin', item);
        }
    });

    let count = 4;
    const counter = setInterval(timer, 1000);
    const hOne = document.querySelector('h1');

    function timer() {
        count -= 1;
        if (count <= 0) {
            clearInterval(counter);
            elem.remove()
        }
        hOne.innerText = count;
    }

    timer();

});

const newArr = ['hello', 'world', 'kiev', 'kharkiv', ['irpin', 'brovary', 'bucha',], 'lviv', 'odessa'];
showList(newArr, 'ul');
