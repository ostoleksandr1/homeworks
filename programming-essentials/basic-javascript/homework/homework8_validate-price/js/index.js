const input = document.createElement('input');
input.classList.add('white');
document.body.append('Price', input);

input.addEventListener('mouseover', ev => {
    ev.target.classList.remove('white', 'red');
    ev.target.classList.add('green');
});

input.addEventListener('mouseout', ev => {
    ev.target.classList.remove('green', 'red');
    ev.target.classList.add('white');
    myValidatePrice();
});

function myValidatePrice() {
    if (Number.isNaN(+input.value) || input.value < 0) {
        if (document.querySelector('p')) {
            input.classList.remove('green', 'white');
            input.classList.add('red');
            input.value = '';
            return;
        } else {
            const err = createErrorPrice();
            document.body.insertAdjacentElement("beforeend", err);
            input.classList.remove('green', 'white');
            input.classList.add('red');
            input.value = '';
        }
    } else if (input.value === '') {
        return;
    } else {
        if (document.querySelector('p')) {
            document.querySelector('p').remove();
        }
        const spn = createSpanAndX();
        document.body.insertAdjacentElement('afterbegin', spn);
        input.value = '';
    }
}

function createSpanAndX() {
    const spn = document.createElement('span');
    spn.innerText = `Текущая цена : ${input.value}`;
    spn.style.display = 'block';
    const btn = document.createElement('button');
    btn.innerText = 'X';
    btn.onclick = (e) => {
        e.target.closest('span').remove();
    };
    spn.append(btn);
    return spn;
}

function createErrorPrice() {
    const err = document.createElement('p');
    err.innerText = 'Please enter correct price';
    return err;
}

