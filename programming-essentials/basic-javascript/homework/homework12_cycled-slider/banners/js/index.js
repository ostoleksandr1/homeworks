const imgCollection = document.querySelectorAll('.image-to-show');
const btnStop = document.querySelector('.stop');
const btnContinue = document.querySelector('.continue');

let currentImg = 1;
let imgInterval = setInterval(imjSiider, 3000);

function imjSiider() {
    if (currentImg < imgCollection.length) {
        imgCollection[currentImg - 1].classList.toggle('viz');
        imgCollection[currentImg].classList.add('viz');
        currentImg++;
    } else {
        imgCollection[imgCollection.length - 1].classList.toggle('viz');
        currentImg = 0;
        imgCollection[currentImg].classList.add('viz');
        currentImg++;
    }
}

btnStop.addEventListener('click', evt => {
    clearInterval(imgInterval);
   btnStop.setAttribute('disabled','disabled');
   btnContinue.removeAttribute('disabled');
});

btnContinue.addEventListener('click', evt => {
    imgInterval = setInterval(car, 3000);
   btnContinue.setAttribute('disabled','disabled');
   btnStop.removeAttribute('disabled');
});