const tabs = document.querySelector('.tabs');
const tabsContent = document.querySelector('.tabs-content');

tabs.addEventListener('click', evt => {
    const ulTabs = tabs.querySelectorAll('li');

    for (elem of ulTabs) {
        elem.classList.toggle('active', false);
    }
    evt.target.classList.add('active');
    let activ = evt.target.dataset.text;

    const ulTabsContent = tabsContent.querySelectorAll('li');

    for (elem of ulTabsContent) {
        if (activ === elem.dataset.text) {
            elem.classList.remove('display-none');
            elem.classList.add('display');
        } else {
            elem.classList.toggle('display', false);
            elem.classList.toggle('display-none', true);
        }
    }
});