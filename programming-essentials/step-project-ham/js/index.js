const serviceMenu = document.querySelector('.service-menu');
const servicePhotoMenu = document.querySelector('.service-photo-menu');

serviceMenu.addEventListener('click', evt => {
  const ulServiceMenuBtn = serviceMenu.querySelectorAll('.a-service');

  for (elem of ulServiceMenuBtn) {
    elem.classList.toggle('active-click', false);
  }
  evt.target.classList.add('active-click');
  let active = evt.target.dataset.text;

  const divActive = servicePhotoMenu.querySelectorAll('.service-wrapper');

  for (elem of divActive) {
    if (active === elem.dataset.text) {
      elem.classList.remove('display-service-none');
      elem.classList.add('display-service')
    } else {
      elem.classList.toggle('display-service', false);
      elem.classList.toggle('display-service-none', true);
    }
  }
})

const generateImages = (elem, count, wid, hit) => {
  const newImage = () => {
    const item = document.createElement("div");
    const content = document.createElement("img");
    const width = wid;
    let height = hit;
    content.src = `https://picsum.photos/${width}/${height}`;
    hit += 1;
    item.appendChild(content);
    return item;
  };
  const images = document.createDocumentFragment();
  for (let i = 0; i < count; i++) images.appendChild(newImage());
  elem.appendChild(images);
};

const divAmazing = document.querySelector('.amazing-pictures');
const btnAmazing = document.querySelector('.btn-amazing');
const btnGallery = document.querySelector('.btn-add');
const divGallery = document.querySelector('.gallery-images');

let btnAddCount = 0;
let btnCount = 0;
btnAmazing.addEventListener('click', evt => {
  generateImages(divAmazing, 12, 285, 206);
  btnCount++;
  if (btnCount === 2) {
    btnAmazing.remove();
  }
})

btnGallery.addEventListener('click', evt => {
  generateImages(divGallery, 12, 378, 290);
  btnAddCount++;
  if (btnAddCount === 1) {
    divGallery.style.maxHeight = '2300px';
  } else if (btnAddCount === 2) {
    divGallery.style.maxHeight = '3600px';
    btnGallery.remove();
  }
})

const amazingMenu = document.querySelector('.amazing-menu');

amazingMenu.addEventListener('click', evt => {
  let active = evt.target.dataset.box;
  const divAmazingActive = divAmazing.querySelectorAll('.amazing-active');
  if (active === 'all') {
    for (elem of divAmazingActive) {
      elem.classList.toggle('display-service-none', false);
    }
  } else {
    for (elem of divAmazingActive) {
      if (active === elem.dataset.box) {
        elem.classList.toggle('display-service-none', false);
      } else {
        elem.classList.toggle('display-service-none', true);
      }
    }
  }
})

const myCarousel = document.querySelector('.my-carousel');
const carousel = document.querySelector('.carousel');
const btnCarouselLeft = carousel.querySelector('.left');
const btnCarouselRight = carousel.querySelector('.right');
const carouselLine = myCarousel.querySelector('.carousel-line');
const smallImages = carousel.querySelector('.small-images');
let motion = 0;

const updateImages = (activeImage, targetImage) => {
  activeImage.classList.remove('active');
  targetImage.classList.add('active');
};

btnCarouselRight.addEventListener('click', evt => {
  const activeImage = smallImages.querySelector('.small.active');
  const nextImage = activeImage.nextElementSibling;
  if (!nextImage) return;
  updateImages(activeImage, nextImage);
  motion += 1160;
  carouselLine.style.left = -motion + 'px';
})

btnCarouselLeft.addEventListener('click', evt => {
  const activeImage = smallImages.querySelector('.small.active');
  const prevImage = activeImage.previousElementSibling;
  if (!prevImage) return;
  updateImages(activeImage, prevImage);
  motion -= 1160;
  carouselLine.style.left = -motion + 'px';
})

smallImages.addEventListener('click', evt => {
  const targetImage = evt.target.closest('li');
  if (!targetImage) return;
  const activeImage = smallImages.querySelector('.small.active');
  updateImages(activeImage, targetImage);

  if (targetImage.classList.contains('alba')) {
    carouselLine.style.left = 0 + 'px';
    motion = 0;
  } else if (targetImage.classList.contains('vasy')) {
    carouselLine.style.left = -1160 + 'px';
    motion = 1160;
  } else if (targetImage.classList.contains('goga')) {
    carouselLine.style.left = -2320 + 'px';
    motion = 2320;
  } else if (targetImage.classList.contains('hasan')) {
    carouselLine.style.left = -3480 + 'px';
    motion = 3480;
  }
})


