const menuBtn = document.querySelector('.menu-btn');
const menuBurger = document.querySelector('.menu-btn__burger');
const body = document.querySelector('body');
const navMenu = document.querySelector('.nav__burger-menu-ul');

let menuOpen = false;
menuBtn.addEventListener('click', () => {
  if (!menuOpen) {
    menuBtn.classList.add('open');
    menuBurger.classList.add('open');
    navMenu.classList.remove('nav__burger-menu-none');
    navMenu.classList.add('nav__burger-menu-active');
    menuOpen = true;
  } else {
    menuBtn.classList.remove('open');
    menuBurger.classList.remove('open');
    navMenu.classList.remove('nav__burger-menu-active');
    navMenu.classList.add('nav__burger-menu-none');
    menuOpen = false;
  }
});

body.onresize = ev => {
  if ('ev', ev.target.innerWidth > 768) {
    menuBtn.classList.remove('open');
    menuBurger.classList.remove('open');
    menuOpen = false;
    navMenu.classList.remove('nav__burger-menu-ul');
    navMenu.classList.toggle('nav__burger-menu-none', false);
    navMenu.classList.toggle('nav__burger-menu-active', false);
  } else {
    navMenu.classList.add('nav__burger-menu-ul');
    navMenu.classList.toggle('nav__burger-menu-none', true);
  }
}