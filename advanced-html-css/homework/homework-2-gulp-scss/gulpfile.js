const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const browserSync = require('browser-sync').create();
const clean = require('gulp-clean');
const cleanCss = require('gulp-clean-css');
const minify = require('gulp-js-minify');
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');
const autoprefixer = require('gulp-autoprefixer');
const imagemin = require('gulp-imagemin');



gulp.task('compress', function () {
  gulp.src('src/img/*')
    .pipe(imagemin())
    .pipe(gulp.dest('dist/img'))
});


gulp.task('clean', function () {
  return gulp.src('dist')
    .pipe(clean());
});

gulp.task('compressJs', function () {
  gulp.src('src/js/*.js')
    .pipe(uglify())
    .pipe(concat('all.js'))
    .pipe(minify())
    .pipe(gulp.dest('dist/js/'))
});

gulp.task('first', function () {
  return gulp.src('src/scss/index.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(concat('all.css'))
    .pipe(autoprefixer({
      cascade: false
    }))
    .pipe(cleanCss())
    .pipe(gulp.dest('dist/css/'))
    .on('end',browserSync.reload);
});

gulp.task('watch', function () {
  gulp.watch('src/scss/**/*.scss', gulp.series('first'));
});

gulp.task('build', gulp.series('clean', gulp.parallel('first', 'compressJs','compress')));

gulp.task('dev', function () {
  browserSync.init({
    server: {
      baseDir: './',
    },
  });
  gulp.watch('./*.html', browserSync.reload);
  gulp.watch(['./src/scss/**/*.scss', '.src/js/*.js'], gulp.series('build'));
});
